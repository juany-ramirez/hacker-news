import React, { useState, useEffect } from 'react';
import { Avatar, ListItemAvatar, ListItem, ListItemText } from '@mui/material';
import { useParams } from 'react-router-dom';
import NewsService from '../services/news';
import { News } from '../models/news.model';

type NewsParams = {
  id: string;
};

const NewsDetail: React.FC = (props) => {
  const { id } = useParams<NewsParams>();
  const newsService = new NewsService();
  const [newsItem, setNewsItem] = useState<News | null>(null);

  useEffect(() => {
    const newsIdNumber: number = +id;
    newsService
      .getNewsItem(newsIdNumber)
      .then((response) => {
        if (response) setNewsItem(response);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    newsItem && (
      <div style={{ color: 'black' }}>
        <h3>{newsItem.title}</h3>
        <ListItem>
          <ListItemAvatar>
            <Avatar alt="Profile Letter">
              <span style={{ textTransform: 'capitalize' }}>
                {newsItem.by?.charAt(0)}
              </span>
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary={newsItem.by} />
        </ListItem>
        <iframe
          style={{ minHeight: '80vh' }}
          title={newsItem.title}
          src={newsItem.url}
          width="100%"
        />
      </div>
    )
  );
};

export default NewsDetail;
