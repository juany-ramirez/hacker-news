import React, { useState, useMemo } from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardContent,
  Avatar,
  ListItemAvatar,
  ListItem,
  ListItemText,
} from '@mui/material';
import { News } from '../models/news.model';
import NewsService from '../services/news';

interface NewsItemProps {
  newsId: number;
}

const NewsItem: React.FC<NewsItemProps> = (props) => {
  const newsService = new NewsService();
  const [newsItem, setNewsItem] = useState<News | null>(null);

  useMemo(() => {
    newsService
      .getNewsItem(props.newsId)
      .then((response) => {
        if (response) setNewsItem(response);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const displayTime = (seconds: number | undefined) => {
    const milliseconds = (seconds || 0) * 1000;
    return new Date(milliseconds).toLocaleDateString('en-US');
  };

  return (
    newsItem && (
      <Link to={`/article/${newsItem.id}`} style={{ textDecoration: 'none' }}>
        <Card sx={{ minWidth: '100%', minHeight: '2rem', my: 2 }}>
          <CardContent>
            <h3>{newsItem.title}</h3>
            <ListItem>
              <ListItemAvatar>
                <Avatar alt="Profile Letter">
                  <span style={{ textTransform: 'capitalize' }}>
                    {newsItem.by?.charAt(0)}
                  </span>
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={newsItem.by}
                secondary={displayTime(newsItem.time)}
              />
            </ListItem>
          </CardContent>
        </Card>
      </Link>
    )
  );
};

export default NewsItem;
