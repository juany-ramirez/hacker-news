import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Container, Pagination, AppBar, Toolbar, Button } from '@mui/material';
import NewsService from './services/news';
import NewsItem from './components/NewsItem';
import NewsDetail from './components/NewsDetail';
import './App.css';

const Home: React.FC = () => {
  const newsService = new NewsService();
  const [newsIds, setNewsIds] = useState<number[]>([]);
  const [selectedIds, setSelectedIds] = useState<number[]>([]);
  useEffect(() => {
    newsService
      .getNewsIds()
      .then((response) => {
        if (response) setNewsIds(response);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const [page, setPage] = useState(1);
  const onPageChange = (event: React.ChangeEvent<unknown>, page: number) => {
    setPage(page);
  };
  useEffect(() => {
    const first = page - 1 * 25;
    const second = page * 25;
    const selectedIds = newsIds.splice(first, second);
    setSelectedIds(selectedIds);
  }, [page, newsIds]);
  return (
    <div>
      {selectedIds.map((id) => {
        return <NewsItem key={id} newsId={id} />;
      })}
      <Pagination
        count={20}
        page={page}
        color="primary"
        onChange={onPageChange}
      />
    </div>
  );
};

function App() {
  return (
    <div className="app">
      <Router>
        <AppBar position="static">
          <Toolbar className="app-toolbar">
            <Link className="link-button" to="/">
              <h2>Hacker News</h2>
            </Link>
            <span>
              <Link className="link-button" to="/">
                <Button color="inherit">Home</Button>
              </Link>
            </span>
          </Toolbar>
        </AppBar>
        <Container className="react-container">
          <Switch>
            <Route path="/article/:id">
              <NewsDetail />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Container>
      </Router>
    </div>
  );
}

export default App;
