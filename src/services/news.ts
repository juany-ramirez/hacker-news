import axios, { AxiosInstance } from 'axios';
import { News } from '../models/news.model';

export default class NewsService {
  axiosIns: AxiosInstance;

  constructor() {
    this.axiosIns = axios.create({
      baseURL: `${process.env.REACT_APP_API}`,
    });
  }

  async getNewsIds(): Promise<null | number[]> {
    try {
      const result = await this.axiosIns.get<number[]>('topstories.json');
      return result.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  async getNewsItem(id: number): Promise<null | News> {
    try {
      const result = await this.axiosIns.get<News>(`item/${id}/.json`);
      return result.data;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}
